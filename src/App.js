import React, {Component} from 'react';
import {connect} from 'react-redux';
import './App.css';
import {createUrl, fetchUrl} from "./store/action";

class App extends Component {
    state = {
      originalUrl: ''
    };

    changeValue = event => {
      this.setState({originalUrl: event.target.value});
    };

    createUrl = event => {
        event.preventDefault();

        this.props.onUrlCreated(this.state).then(() => {
            this.setState({originalUrl: ""});
        });
    };

    redirect = () => {
        this.props.onUrlFetched(this.props.shortUrl)
    };

    render() {

        return (
            <div className="App">
                <h1>Shorten your link!</h1>

                <form action="#" onSubmit={this.createUrl}>
                    <input type="text"
                           placeholder="Enter your url here"
                           value={this.state.originalUrl}
                           onChange={this.changeValue}
                           required
                    />
                    <button>Shorten</button>
                </form>

                {this.props.shortUrl && <div>
                    <h4>Shorten url: </h4>
                    <a href={'http://localhost:8000/links/' + this.props.shortUrl}
                       target="_blank" onClick={this.redirect}
                    >{'http://localhost:8000/links/' + this.props.shortUrl}</a>
                </div>}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        shortUrl: state.shortUrl
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onUrlCreated: url => dispatch(createUrl(url)),
        onUrlFetched: shortUrl => dispatch(fetchUrl(shortUrl))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
