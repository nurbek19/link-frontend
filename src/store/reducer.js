import {CREATE_URL_SUCCESS} from "./action";

const initialState = {
    shortUrl: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_URL_SUCCESS:
            return {...state, shortUrl: action.urlData.shortURL};
        default:
            return state;
    }
};

export default reducer;