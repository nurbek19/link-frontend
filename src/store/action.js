import axios from '../axios-api';

export const CREATE_URL_SUCCESS = 'CREATE_URL_SUCCESS';
export const FETCH_SHORT_URL = 'FETCH_SHORT_URL';

export const createUrlSuccess = urlData => {
  return {type: CREATE_URL_SUCCESS, urlData}
};

export const createUrl = url => {
  return dispatch => {
      return axios.post('/links', url).then(response => {
          dispatch(createUrlSuccess(response.data));
      })
  }
};

export const fetchShortUrl = () => {
  return {type: FETCH_SHORT_URL}
};

export const fetchUrl = shortUrl => {
  return dispatch => {
      return axios.get(`/links/${shortUrl}`).then(() => {
         dispatch(fetchShortUrl());
      });
  }
};